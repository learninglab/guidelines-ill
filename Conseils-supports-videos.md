## Conseils pour la réalisation de vidéos pédagogiques

### 1. Généralités vidéos pédagogiques 

* Faire des vidéos courtes (plutôt autour de 5-6')
* "Une vidéo = une notion" (par ex. dans un contexte d'un Mooc qui contient une série de vidéos)

**Note :**

                Comme il n'y a pas d'interactivité dans une vidéo par rapport à un cours en présentiel, 
                on décroche plus facilement devant une vidéo ... 
                donc préférez plutôt une série de petites vidéos qu'une "longue vidéo".
### 2. Format et présentation des slides pour la vidéo: homogénéité et lisibilité.

* Slides à réaliser au format 16/9e (format de vidéo)
* La numérotation des slides est intéressante car elle sert de référence pouvant être utilisée par les étudiants lorsqu’ils posent des questions ou par l’enseignant. 
* Préférer si possible une présentation très visuelle, peu d'écrit par slides , faites plutôt des slides aérées avec des paragraphes bien marqués.
* Essayer d'illustrer votre texte : 📌 _"Ça peut paraître évident, mais il apparaît qu’apprendre avec des images et du texte ensemble, que ce soit des animations avec narration ou des images statiques avec du texte, est mieux que d’apprendre avec du texte seul."_  _[Le numérique va révolutionner l’éducation … vraiment ? Binaire, juillet 2020](https://www.lemonde.fr/blog/binaire/2020/07/10/le-numerique-va-revolutionner-leducation-vraiment/)_
* Si incrustation de l'orateur en même temps que les slides, pensez à réserver un espace libre (par exemple en bas à droite de la slide pour faire l'inscrustation de votre image). [(📌 ex. le modèle MOOC ILL)](140806-Templates_Moocs-zonesreservéesvisibles.pdf)

Pour une vidéo faisant partie d'un ensemble dans le cadre d'un cours ou d'un mooc par exemple :

* Faire une présentation du cours et de son plan dans une 1ère séquence vidéo introductive
* Donner des points de repères sur la place de la vidéo si elle fait partie d'une série :  penser que la vidéo pourra être téléchargée et pourra être vue "hors de son contexte" 

  📌 Exemple : [un modèle de présentation avec quelques conseils de l'EPFL ](SlideTemplateEPFL.pdf)

#### ▶️ Quelques conseils à connaître : 

**a) Structuration et définition du contenu :**

- Une slide pour une idée
- En moyenne, on évalue à 1 min la présentation d’une slide
- Respecter la règle des 6 (pas plus de 6 lignes, 6 mots, 6 puces par slide)
- Utiliser des mots-clés
- Formuler des _Take Home Messages_ = **informations à retenir**

**KISS : Keep It Straight and Simple !**

**b) Présentation visuelle des informations**
- Mettre en évidence les points-clés 
- Ne pas écrire de paragraphes de textes mais présenter les idées de manière synthétique sous forme de listes à puces 
- Éviter les signes de ponctuation en fin de phrase dans les listes à puces

**c) Utilisation des illustrations**

- Veiller à la qualité et à la lisibilité des illustrations et graphiques utilisés
- Utiliser un maximum d’images en veillant à ce qu’elles :
  -  soient en rapport avec le message présenté
  -  ne soient pas là uniquement pour décorer 
  -  servent à renforcer ou compléter le message présenté 
  -  illustrent et expliquent
  -  plutôt 2 images par slide
  -  plutôt un graphique par slide

**A picture can say more than a thousand words !**

**d) forme du discours** 
        
- Parler de façon enthousiaste et assez rapidement. 
- Regarder la caméra comme si elle représentait l’auditoire : c’est à elle que l’on s’adresse.

### 3. Quelques choix de présentation de vos videos

#### a) Slides + enregistrement voix off (modèle "tuto")
* Plus facile à réaliser : utilise des outils "basics"  : enregistrement voix et capture écran
* Importance de la qualité du support qui doit être bien fait et animé pour retenir l'attention
* Importance de la qualité du son : s'équiper d'un bon micro.  

#### b) Alternance supports et orateur 

* Alternance entre enregistrement webcam et screencast
* Vous variez les plans de votre vidéo
* Vous mettez l'importance soit sur l'écoute de l'orateur, soit sur ce que vous souhaitez montrer
* Nécessite du montage vidéo pour enchainer les différents plans (ex. _Camtasia_ (windows, apple (payant), _OBS Studio_ (windows, Apple, Linux (libre)), etc.)

#### c) Présence de l'orateur avec la présentation des slides

* Enregistrement simultanée video webcam et screencast
* Présence de l'image de l'orateur dans la présentation peut aider à maintenir l'attention  
* Bien synchroniser/aligner le discours avec ce qui est écrit sur la slide
* La présentation écrite doit bien aider à la compréhension de la parole de l'orateur. (voir recommandations sur les supports écrits)
...

Nombreux tutos sur youtube, en particulier pour OBS Studio :
- [Using OBS Studio for Screen Recordings](https://www.youtube.com/watch?v=NycDnsXb1fc)
- [Enregistrer des vidéos de votre écran, Webcam, écran virtuel avec OBS sur votre PC,Mac ou Linux](https://www.youtube.com/watch?v=gOD39lrppoU)

### 4. Autres sujets importants sur la vidéo : 

#### a) Propriété intellectuelle : pensez-y!

* Pensez à choisir une licence pour votre vidéo et mettez-la sur la vidéo : par exemple choisir une licence 📌 [Creative Commons ](https://creativecommons.org/share-your-work/)
* S’assurer des droits de diffusion des images utilisées : si utilisation d'images trouvées sur le web, faire bien attention à la licence de ces images/photos : si vous ne voulez pas avoir de soucis choisir des images avec des licences sans droits en cherchant sur les sites  wikicommons par exemple ou pixabay 

#### b) Sous-titres vidéo 

- Important pour l'accessibilité numérique 
- Une façon de faire sous-titrer la vidéo "automatiquement" (mais cela demande pas mal de retouches ...), déposez-la sur Youtube en mode privé
si vous ne souhaitez pas ouvrir la lecture à tous.

 Un bonne ressource sur le sujet : 📌 [Liste de bonnes pratiques sous-titrage web](https://gist.github.com/knarf18/76f3738e970e8efa9243)

### Conclusion : pensez "Objectifs pédagogiques" !

* N'hésitez pas à faire une introduction sous forme d'objectifs pédagogiques 
_"dans cette vidéo vous allez apprendre à ... et vous pourrez appliquer  ... "_ 
  et une conclusion avec "ce que vous avez appris dans cette "vidéo"

À propos d'objectifs pédagogiques, vous pouvez consulter la vidéo sur le site d'Inria Learning Lab : 📌 [Définir les objectifs pédagogiques de son MOOC](https://learninglab.inria.fr/accompagnement-pedagogique) 
            











